const expect = require("chai/lib/chai/interface/expect")
const { element } = require("nightwatch/lib/api/_loaders/element-global")
const { elementTextIs } = require("selenium-webdriver/lib/until")

module.exports = {
    'Designory Location Test': async function (browser) {
        browser
            .url("https://www.designory.com/locations/chicago")
            .pause(1 * 1000)}

            .expect.element('h1').text.to.equal ('CHI')
            .assert.containstext('#body > div.container.location-detail > div > div.grid-12.grid-md-5.grid-xl-6 > div > div:nth-child(2) > div > p', "312 729 4500")
            .getElementSize(H2, 40)
            .expect.element('location') .assert.containstext("https://maps.googleapis.com/maps/api/staticmap?center= 225 N Michigan Ave, Suite 700 Chicago, IL 60601&zoom=16.5&markers= 225 N Michigan Ave, Suite 700 Chicago, IL 60601&size=400x150&key=AIzaSyAkAXQMgbxLGj6ZFqVAAR8JT4-5LaWKfIY")
    }            

            

    


    