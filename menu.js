module.exports = {
    'Designory Menu Tests': function (browser) {
        browser
            .url("http://designory.com/")
            .pause(1 * 1000)
            .click("#nav-toggle > button")
            .assert.containsText("#nav-toggle", "work")
            .assert.containsText(".#nav-toggle", "about")
            .assert.containsText("#nav-toggle", "careers")
            .assert.containsText("#nav-toggle", "locations")
            .assert.containsText("#nav-toggle", "contact")
            .assert.containsText("#nav-toggle", "news")
            .click("#body > div.nav-wrapper.scroll > nav > ul > li.active > a")
            .assert.containsText("Work")
            .click("#nav-toggle > button")
            .click("#body > div.nav-wrapper.scroll > nav > ul > li.active > a")
            .assert.containsText("About")
            .click("#nav-toggle > button")
            .click("#body > div.nav-wrapper.scroll > nav > ul > li.active > a")
            .assert.containsText("Careers")
            .click("#nav-toggle > button")
            .click("#body > div.nav-wrapper.scroll > nav > ul > li.active > a")
            .assert.containsText("Locations")
            .click("#nav-toggle > button")
            .click("#body > div.nav-wrapper.scroll > nav > ul > li.active > a")
            .assert.containsText("Contact")
            .click("#nav-toggle > button")
            .click("#body > div.nav-wrapper.scroll > nav > ul > li.active > a")
            .assert.containsText("News")
      
    }

}