var expect = require('chai').expect;

module.exports = {
  'Navigation': function (browser) {
    var expectedNavElements = ['WORK', 'ABOUT', 'CAREERS', 'LOCATIONS', 'CONTACT', 'NEWS'];

    function testNavElements(elements) {
      elements.value.forEach(function (element, index) {
        browser.elementIdText(element.ELEMENT, function(res) {
          expect(res.value).to.equal(expectedNavElements[index]);
        });
      });
    }

    browser
      .url('http://designory.com') 
      .waitForElementVisible('#nav-toggle', 1000);

    browser.expect.element('#nav-toggle').to.be.present;
    browser.elements('css selector', '#nav > .item', testNavElements);

    browser.end();
  }
};