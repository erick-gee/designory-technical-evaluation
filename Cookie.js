module.exports = {
    'Designory Cookie Tests': function (browser) {
        browser
            .url("http://designory.com/")
            .pause(1 * 1000)
            .click("#nav-toggle > button")

            const acceptCookie = "#cookie-container > div > button"
            browser.click(acceptCookie)
            .pause(1 * 1000)
            browser.deleteCookies([callback])

            .assert.containsText("#cookie-container > div > button", "Ok, got it")



            .assert.containsText("#nav-toggle", "work")
            .assert.containsText(".#nav-toggle", "about")
            .assert.containsText("#nav-toggle", "careers")
            .assert.containsText("#nav-toggle", "locations")
            .assert.containsText("#nav-toggle", "contact")
            .assert.containsText("#nav-toggle", "news")}
    }